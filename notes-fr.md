# qmlLogo

Quelques explications sur le codage de **qmlLogo**.

## L'évaluateur

Le Logo est un langage interprété, qui fait partie de la famille des **évaluateurs**.

### Vocabulaire

Un programme Logo est une suite de **mots** qui sont évalués de façon séquentielle.\
Les mots peuvent être regroupés dans des **blocs** entourés de crochets.

### Pré-traitement

Le code source est transformé en un arbre.\
C'est codé en javascript sous la forme d'un tableau, dont les éléments sont des mots ou des tableaux.

Un code comme celui-ci :

    HOME CLEARTEXT
    REPEAT 4 [
        FORWARD 120
        RIGHT 90
        PRINT [ XCOR YCOR HEADING ]
    ]

Identique à celui-là :

    HOME CLEARTEXT REPEAT 4 [ FORWARD 120 RIGHT 90 PRINT [ XCOR YCOR HEADING ] ]

est converti en arbre par la fonction `buildTree()` :

    [
        "HOME",
        "CLEARTEXT",
        "REPEAT",
        "4",
        [
            "FORWARD",
            "120",
            "RIGHT",
            "90",
            "PRINT",
            [
                "XCOR",
                "YCOR",
                "HEADING"
            ]
        ]
    ]


### Evaluation

La fonction principale s'appelle `evaluate()`, elle reçoit toujours un arbre en paramètre (un tableau javascript).\
Tous les mots du code source passeront un à un dans cette fonction.\
Elle extrait le premier élément de l'arbre, identifie sa nature et cherche à l'évaluer et extrayant des éléments suivants si besoin.\
Si ce premier élément est un tableau `evaluate` appelle `evaluate` sur le sous-arbre.

Exemple avec une addition. Le Logo utilise la [notation polonaise](https://fr.wikipedia.org/wiki/Notations_infix%C3%A9e,_pr%C3%A9fix%C3%A9e,_polonaise_et_postfix%C3%A9e).

    SUM 12 5

`evaluate` reconnait le mot `SUM` du langage qui attend deux paramètres. Ensuite `evaluate` est appelé sur les mots `12` et `5` avant de réaliser l'addition.

Pour additionner trois chiffres, on fait comme ça : `SUM 5 SUM 2 5`

### Typage des données

Les évaluateurs utilisent souvent un typage fort des données (int, real, string) voire des types complexes comme les `tuple` (une adresse IP est un tuple, ou une couleur sous la forme `127.255.255`.\
J'ai choisi de n'utiliser aucun typage intermédiaire (juste pour voir si ça marchait) et de ne conserver que des chaines de caractères : `[ "SUM" "12" "5" ]` plutôt que `[ "SUM" 12 5 ]`.\
Le 12 et le 5 ne sont transformés en valeur numérique qu'au dernier moment avant l'addition.\
Ce choix n'ayant pas posé de problème jusqu'à maintenant, il est resté.

## Organisation du code

### La tortue

Une tortue de base est faite de très peu de paramètres : position, cap, couleur et épaisseur du trait.\
La seule fonction compliquée, c'est celle qui trace un trait, il faut un tout petit peu de trigonométrie.

La première version que j'ai fait produisait du `SVG`.\
La seconde utilisait les [Canvas javascript](https://developer.mozilla.org/fr/docs/Web/API/Canvas_API) qui fonctionnent en Qml.\
Maintenant le dessin est fait avec les outils natifs Qml [Shape](https://doc.qt.io/qt-5/qml-qtquick-shapes-shape.html) et [ShapePath](https://doc.qt.io/qt-5/qml-qtquick-shapes-shapepath.html).\
En gros, il ne faut que ré-écrire la fonction `forward()`. Ici c'est un peu plus compliqué pour fabriquer des animations (le trait qui avance avec la tortue).

### Le langage Logo

On commence par la fin du code où un grand tableau permet d'associer les mots du langage avec les fonctions javascript correspondantes. Pas de paramètre à cet endroit, juste le nom de la fonction.

Les fonctions correspondants à un mot du langage finissent toutes par un caractère souligné et recoivent toujours le même paramètre  `tree`, la suite de l'arbre à évaluer.

En remontant dans le code, on trouve les fonctions arthmétiques et logiques.\
Plus haut, les fonctions de sortie texte, puis les structures de contrôle.

Au début se trouvent les fonctions internes à l'évaluateur.

## Asynchronisme

Les premières versions étaient synchrones, l'évaluateur rendait la main une fois le dessin terminé.\
Ce code n'a pas résisté à la première boucle infinie `WHILE [ "TRUE ] [...]`.\
Pas moyen de reprendre la main, ni de tuer le script, argh.

Après une longue promenade dans les documentations javascript, j'ai trouvé la solution avec les [Generator function](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction), et les opérateurs [yield](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield) et [yield*](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield*) qui permettent d'interrompre et de reprendre une fonction là où on l'avait laissée.

Ca s'utilise comme ça :\
Dans la fonction `startCode()`, on crée un itérateur `codeIterator` sur la fonction principale `mainLoop()` qui s'arrêtera au premier `yield` ou `return` rencontré. Ensuite on fait séquentiellement `codeIterator.next()` pour exécuter l'étape suivante. Le `next()` se charge de descendre dans l'arborescence du code et de revenir des fonctions appelées.\
Quand les `next()` sont finis, `codeIterator` renvoi `done`.

## Commentaires

Au bout du compte, le code de `Logo.qml` est un gros tas de `yield* evaluate(tree)`et de `tree.shift()` assez indigeste.\
Même si les fonctions sont bien rangées à la fin, on ne peut pas dire que ce soit très lisible.
