# qmlLogo

Some explanations on the coding of **qmlLogo**.

## The Evaluator

Logo is an interpreted language, part of the **evaluator** family.

### Vocabulary

A Logo program is a sequence of **words** that are evaluated sequentially.\
Words can be grouped into **blocks** surrounded by square brackets.

### Pre-processing

The source code is transformed into a tree.\
This is coded in javascript as an array, whose elements are words or arrays.

A code like this one:

    HOME CLEARTEXT
    REPEAT 4 [
        FORWARD 120
        RIGHT 90
        PRINT [ XCOR YCOR HEADING ]
    ]

Same as above:

    HOME CLEARTEXT REPEAT 4 [ FORWARD 120 RIGHT 90 PRINT [ XCOR YCOR HEADING ] ]

is converted to a tree by the buildTree()` function:

    [
        "HOME",
        "CLEARTEXT",
        "REPEAT",
        "4",
        [
            "FORWARD",
            "120",
            "RIGHT",
            "90",
            "PRINT",
            [
                "XCOR",
                "YCOR",
                "HEADING"
            ]
        ]
    ]


### Evaluation

The main function is called `evaluate()`, it always receives a tree as a parameter (a javascript array).\
All the words in the source code will be passed one by one to this function.\
It extracts the first element of the tree, identifies its nature and seeks to evaluate it, extracting subsequent elements if necessary.\
If this first element is an array `evaluate` calls `evaluate` on the subtree.

Example with an addition. The Logo uses [Polish notation](https://en.wikipedia.org/wiki/Polish_notation).

    SUM 12 5

`evaluate` recognizes the word `SUM` from the language which expects two parameters. Then `evaluate` is called on the words `12` and `5` before performing the addition.

To add three digits, we do this: `SUM 5 SUM 2 5`

### Data typing

Evaluators often use strong data typing (int, real, string) or even complex types such as `tuple` (an IP address is a tuple, or a colour in the form `127.255.255`.\
I chose not to use any intermediate typing (just to see if it worked) and to keep only strings: `["SUM" "12" "5" ]` rather than `["SUM" 12 5 ]`.\
The 12 and 5 are only transformed into numerical values at the last moment before the addition.\
As this choice has not caused any problems so far, it has remained.

## Organization of the code

### The turtle

A basic turtle is made of very few parameters: position, heading, colour and line thickness.
The only complicated function is the one that draws a line, it requires a little bit of trigonometry.

The first version I did produced `SVG`.\
The second one used [Canvas javascript](https://developer.mozilla.org/fr/docs/Web/API/Canvas_API) which works in Qml.\
Now the drawing is done with the native Qml tools [Shape](https://doc.qt.io/qt-5/qml-qtquick-shapes-shape.html) and [ShapePath](https://doc.qt.io/qt-5/qml-qtquick-shapes-shapepath.html).\
Basically, you just need to rewrite the `forward()` function. Here it's a bit more complicated to make animations (the line that moves forward with the turtle).

### The Logo language

We start with the end of the code where a large table allows to associate the words of the language with the corresponding javascript functions. No parameter here, just the name of the function.

The functions corresponding to a language word all end with an underlined character and always receive the same `tree` parameter, the continuation of the tree to be evaluated.

Moving up in the code, we find the arthmetic and logical functions.\
Further up are the text output functions, and then the control structures.

At the beginning are the internal functions of the evaluator.

## Asynchronism

The first versions were synchronous, the evaluator would hand over once the drawing was finished.\
This code did not survive the first infinite loop `WHILE ["TRUE ] [...]`.\
No way to take over, no way to kill the script, argh.

After a long walk in the javascript documentations, I found the solution with the [Generator function](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction), and the [yield](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield) and [yield*](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield*) operators which allow to interrupt and resume a function where we left it.

It's used like this:\
In the `startCode()` function, we create a `codeIterator` iterator on the main `mainLoop()` function which will stop at the first `yield` or `return` encountered. Then we sequentially do `codeIterator.next()` to execute the next step. The `next()` takes care of going down the code tree and returning from the called functions.\
When the `next()` is finished, `codeIterator` returns `done`.

## Comments

In the end, the `Logo.qml` code is a big pile of `yield* evaluate(tree)` and `tree.shift()` that is rather indigestible.\
Even if the functions are neatly arranged at the end, it's not very readable.

Translated with www.DeepL.com/Translator (free version)
