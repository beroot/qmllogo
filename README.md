# qmlLogo

**qmlLogo** is a QML based Logo language.\
It's a simple mockup for future developments.

[[_TOC_]]

## Getting started

**qmlLogo** doesn't need compilation now, but you need Qt/Qml development tools installed.\
Try it :
- With QtCreator : Open `LogoLanguage.qmlproject`, set `QMLScene's command line argument` to `--multisample` in the **Projects** tab and run.
- With command line :

```
cd qmllogo/
qmlscene --multisample DemoLogo.qml
```

`--multisample` Enable multisampling (OpenGL anti-aliasing) to get nice drawings.

## Goal
**qmlLogo** try to implement a subset of [UCBLogo](https://people.eecs.berkeley.edu/~bh/logo.html) to be integrated
in GCompris'activities.\
Full UCBLogo compatibility is not required.

The examples provided are a test set for Logo features.

## State
Ready for basic use.\
A list of implemented keywords is available at the end of `Logo.qml`.

### Contains the basic elements of the language
- A LOGO evaluator with asynchronous operation.
- A turtle and its commands set for drawing.
- Text output commands.
- Global and local variables.
- Basic arithmetic and logic operators.
- Arithmetic comparators (EQUAL?, GREATER? etc).
- IF, IFELSE tests
- Loop structures: REPEAT, WHILE, UNTIL.
- Procedures with parameters.
- Basic error handling.

### Still missing
- Many LOGO keywords.

Developer's notes in files `notes.md` or `notes-fr.md`.

## Screenshots (2023-03-11)

![Screenshot 1](logo-screen-1.png)

![Screenshot 2](logo-screen-2.png)

![Screenshot 3](logo-screen-3.png)

## License
**qmlLogo** is free software released under the [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.html).

## Project status
**WORK IN PROGRESS**
