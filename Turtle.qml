/* Turtle.qml
 *
 * Copyright (C) 2023 Bruno ANSELME <be.root@free.fr>
 *
 * Authors:
 *   Bruno ANSELME <be.root@free.fr> (Qt Quick native)
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
import QtQuick 2.12
import QtQuick.Shapes 1.15
import "svgColors.js" as SvgColors

Rectangle {
    id: turtle
    clip: true

    // Turtle's parameters
    property real xPos: 0                   // Turtle x position
    property real yPos: 0                   // Turtle y position
    property var penColor: [ 0, 0, 0 ]
    property int penSize: 1
    property int heading: 0
    property bool isDown: true
    property bool isShown: true
    property bool animate: true
    property real speed: 2.4
    property int interval: 100              // Computed before each move, for a smooth translation.
    property bool running: angleAnimate.running || xAnimate.running || yAnimate.running
    property bool moving: xAnimate.running || yAnimate.running
    property bool startShapePath: true      // if penColor or penSize have changed, start a new ShapePath
    property var currentLine: null
    // Standard logo colors names
    property var colorNames: [ "black", "blue", "green", "cyan","red", "magenta", "yellow", "white", "brown", "tan", "green", "aqua", "salmon", "purple", "orange", "gray" ]

    Shape {     // Each drawing (ShapePath) will be added in this Shape (mainShape.data)
        id: mainShape
        property alias translate: translate
        anchors.fill: parent
        transform: Translate {
            id: translate
            x: width / 2
            y: height / 2
        }
    }

    Shape {     // A single line to animate current drawing
        id: lineShape
        anchors.fill: parent
        transform: translate
        visible: false
        ShapePath {
            id: lineShapePath
            strokeWidth: penSize
            strokeColor: Qt.rgba(penColor[0] / 255, penColor[1] / 255, penColor[2] / 255, 1)
            startX: 0
            startY: 0
            PathLine {
                id: pathLine
                x: xPos
                y: yPos
                Behavior on x {
                    id: xLineMove
                    enabled: animate
                    NumberAnimation { duration: interval }
                }
                Behavior on y {
                    id: yLineMove
                    enabled: animate
                    NumberAnimation { duration: interval }
                }
            }
        }
    }

    Image {
        id: turtleImage
        source: "tux_top_north.svg"
        opacity: isDown ? 1 : 0.4
        visible: animate && isShown
        x: turtle.xPos
        y: turtle.yPos
        transform: [
            Rotation {
                origin.x: turtleImage.width / 2
                origin.y: 0
                angle: heading
                Behavior on angle {
                    id: angleAnimation
                    enabled: animate
                    NumberAnimation { id: angleAnimate }
                }
            },
            Translate {
                x: mainShape.translate.x - turtleImage.width / 2
                y: mainShape.translate.y
            }
        ]
        Behavior on x {
            id: xMove
            enabled: animate
            NumberAnimation {
                id: xAnimate
                duration: interval
            }
        }
        Behavior on y {
            id: yMove
            enabled: animate
            NumberAnimation {
                id: yAnimate
                duration: interval
            }
        }
    }

    onMovingChanged: if (!moving) pushLine()        // Animation is finished

    // Utility functions
    function deg2rad(deg) {
        return deg * Math.PI / 180.0
    }
    function rad2deg(rad) {
        return rad * 180.0 / Math.PI
    }
    function toHex(d) {
        return  ("0"+(Number(d).toString(16))).slice(-2).toUpperCase()
    }
    function distance(x1, y1, x2, y2) {
        var dx = x2 - x1
        var dy = y2 - y1
        return Math.sqrt((dx * dx) + (dy * dy))
    }

    // Turtle Sprite
    function showTurtle() {
        isShown = true
        return ""
    }
    function hideTurtle() {
        isShown = false
        return ""
    }

    // Turtle functions
    function setHeading(heading_) {
        var memAnim = animate
        animate = false
        heading = heading % 360
        animate = memAnim
        heading = Number(heading_)
        return ""
    }
    function getHeading() {
        var rest = heading % 360
        if (rest < 0) rest += 360
        return String(rest)
    }
    function turnRight(angle) {
        angleAnimate.duration = animate ? Math.abs(1000 * angle / Math.pow(10, speed)) : 1    // Compute animation speed
        heading = heading + Number(angle)
        return ""
    }
    function turnLeft(angle) {
        angleAnimate.duration = animate ? Math.abs(1000 * angle / Math.pow(10, speed)) : 1    // Compute animation speed
        heading = heading - Number(angle)
        return ""
    }
    function back(dist) {
        forward(-Number(dist))
        return ""
    }
    function setPenWidth(s) {
        penSize = Number(s)
        startShapePath = true
        return String(s)
    }
    function penUp() {
        isDown = false
        return ""
    }
    function penDown() {
        isDown = true
        return ""
    }
    function setXY(newX, newY) {
        var memAnim = animate
        animate = false
        var dist = distance(xPos, yPos, newX, newY)
        interval = memAnim ? Math.abs(1000 * dist / Math.pow(10, speed)) : 1    // Compute animation speed before setting xPos and yPos
        animate = memAnim
        lineShapePath.startX = xPos = newX
        lineShapePath.startY = yPos = newY
        startShapePath = true
        return ""
    }
    function xCor() {
        return Math.round(xPos)
    }
    function yCor() {
        return Math.round(yPos)
    }
    // Color functions
    function getNamedColor(name) {
        var colHexa = SvgColors.colors[name.toUpperCase()]
        if (colHexa === undefined)
            return [0, 0, 0]
        else
            return [ parseInt(colHexa.substr(0, 2), 16), parseInt(colHexa.substr(2, 2), 16), parseInt(colHexa.substr(4, 2), 16) ]
    }
    function getIndexedColor(idx) {
        return getNamedColor(colorNames[idx % 16])          // Standard LOGO colors
//        var svgColNames = Object.keys(SvgColors.colors)     // Index on SvgColors
//        return getNamedColor(Object.keys(SvgColors.colors)[idx % svgColNames.length])
    }
    function setPenColor(r,g,b) {
        penColor = [ r % 256, g % 256, b % 256 ]
        startShapePath = true
        return ""
    }
    function setBackgroundColor(r, g, b) {
        color = Qt.rgba(r / 255, g / 255, b / 255, 1)
        return ""
    }
    function getBackgroundColor(r, g, b) {
        var col = String(parent.color)
        return [ String(parseInt(col.substr(1, 2), 16)), String(parseInt(col.substr(3, 2), 16)), String(parseInt(col.substr(5, 2), 16)) ]
    }

    function clean() {
        setBackgroundColor(255, 255, 255)
        lineShape.visible = false
        mainShape.data = []
        startShapePath = true
        var shapePath = getShapePath()      // Add an empty move to trigger page repaint
        currentLine = Qt.createQmlObject('import QtQuick 2.12; PathMove {x: ' + xPos + '; y: ' + yPos + '}', shapePath)
        pushLine()
        return ""
    }

    function clearScreen() {
        clean()
        home()
        return ""
    }

    function home() {
        var memAnim = animate
        animate = false
        hideTurtle()
        setXY(0, 0)
        setHeading(0)
        penColor = [ 0, 0, 0 ]
        penSize = 1
        isDown = true
        showTurtle()
        animate = memAnim
        return ""
    }

    // Start a new ShapePath if startShapePath is true else returns last shape.data's ShapePath
    function getShapePath() {
        var shapePath
        if (startShapePath) {           // Create a new ShapePath
            shapePath = Qt.createQmlObject('import QtQuick 2.12;import QtQuick.Shapes 1.15; ShapePath {}', mainShape)
            shapePath.strokeColor = Qt.rgba(penColor[0] / 255, penColor[1] / 255, penColor[2] / 255, 1)
            shapePath.strokeWidth = penSize
            shapePath.fillColor= "transparent"
            shapePath.capStyle = ShapePath.RoundCap
            shapePath.joinStyle = ShapePath.RoundJoin
            shapePath.startX = xPos
            shapePath.startY = yPos
            mainShape.data.push(shapePath)
            startShapePath = false
        } else {                        // Or use last created shapePath
            shapePath = mainShape.data[mainShape.data.length - 1]
        }
        return shapePath
    }

    function pushLine() {           // Push currentLine into path elements
        lineShape.visible = false
        if (currentLine === null) return
        lineShapePath.startX = xPos
        lineShapePath.startY = yPos
        var shapePath = getShapePath()
        shapePath.pathElements.push(currentLine)
        currentLine = null
    }

    function forward(dist) {
        var xNew = xPos + (Math.cos(deg2rad(heading - 90)) * dist)
        var yNew = yPos + (Math.sin(deg2rad(heading - 90)) * dist)
        var shapePath = getShapePath()
        // Line is stored in currentLine and will be pushed in the ShapePath when the animation is finished
        if (isDown) {
            lineShape.visible = true
            currentLine = Qt.createQmlObject('import QtQuick 2.12; PathLine {x: ' + xNew + '; y: ' + yNew + '}', shapePath)
        } else {
            currentLine = Qt.createQmlObject('import QtQuick 2.12; PathMove {x: ' + xNew + '; y: ' + yNew + '}', shapePath)
        }
        // move to new position (this will start animation)
        interval = animate ? Math.abs(1000 * dist / Math.pow(10, speed)) : 1    // Compute animation speed before setting xPos and yPos
        xPos = xNew
        yPos = yNew
        if (!animate) pushLine()    // If no animation push line now into ShapePath
        return ""
    }

    function circle(ray) {
        if (isDown) {
            var duration = animate ? Math.floor(Math.abs(1000 * 6 * ray / Math.pow(10, speed))) : 1
            var behaviorStr = ''
            var sweepAngle = 360
            if (animate) {
                behaviorStr = 'Behavior on sweepAngle { NumberAnimation { duration: ' + duration + ' } }'
                sweepAngle = 0
            }
            var shapePath = getShapePath()
            var circle = Qt.createQmlObject(
                        'import QtQuick 2.12; PathAngleArc {centerX: ' + xPos +
                        ';centerY: ' + yPos +
                        ';radiusX: ' + ray +
                        ';radiusY: ' + ray +
                        ';startAngle: ' + (heading - 90) + '; sweepAngle: ' + sweepAngle + '\n' +
                        behaviorStr + '}', shapePath)
            shapePath.pathElements.push(circle)
            if (startShapePath)
                mainShape.data.push(shapePath)
            angleAnimate.duration = duration
            heading += 360
            if (animate) {
                circle.sweepAngle += 360        // Start the animation of the drawn circle
                angleAnimate.start()            // Start the rotation of the turtle image
            }
            startShapePath = true
        }
        return ""
    }
}

