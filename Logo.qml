/* Logo.qml
 *
 * Copyright (C) 2023 Bruno ANSELME <be.root@free.fr>
 *
 * Authors:
 *   Bruno ANSELME <be.root@free.fr> (Qt Quick native)
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 * References : https://github.com/thisandagain/logo
 *            : https://github.com/inexorabletash/jslogo, ça se teste ici : https://inexorabletash.github.io/jslogo/
 *            : https://github.com/drewish/logo_js
 *            : https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction
 *            : https://www.algo.be/logo1/pdf/man-MSWLogo-fr-an.pdf
*/

import QtQuick 2.12

Item {
    id: logo
    required property var turtle
    required property var output
    property alias interval: scheduler.interval
    property alias scheduler: scheduler
    property bool paused: false
    property bool running: false

    readonly property var regexIdentifier: /^(\.?[A-Za-z][A-Za-z0-9_.\?]*)$/
    readonly property var regexStringLiteral: /^("[^ \[\]\(\)]*)$/
    readonly property var regexVariableLiteral: /^(:[A-Za-z][A-Za-z0-9_]*)$/
    readonly property var regexNumberLiteral: /^(-?[0-9]*\.?[0-9]+(?:[eE]\s*[\-+]?\s*[0-9]+)?)$/

    property var varStack: [{}]         // A stack to store program's variables.
                                        // Each element is an object with variables'name and value for each procedure's depth.
    property var procedures: ({})       // A place to store program's procedures
                                        // Object with procedures'name and code
    property var language: ({})         // Logo language functions
    property var codeIterator: null

    Timer {
        id: scheduler
        interval: 1
        repeat: true
        running: false
        onTriggered: schedule()
    }
    Timer {
        id: sleeper
        interval: 1
        repeat: false
        running: false
        onTriggered: scheduler.start()
    }

    //--------------------------------------------------------
    // Logo parser functions

    // Build a tree structure from logo code
    function buildTree(words) {
        var tree = []
        while (words.length) {
          var atom = words.shift();
          if (atom === '[') {
              tree.push(buildTree(words))
          } else if (atom === ']') {
              return tree
          } else tree.push(atom)
        }
        return tree
    }

    // Deep copy for nested arrays (blk must contains strings or arrays, doesn't work with objects)
    function copyBlock(blk) {
        return JSON.parse(JSON.stringify(blk))
    }

    // Returns a number from atom or throw an error
    function toNumber(atom) {
        if (isNaN(atom)) throw new Error(atom + " " + qsTr("is not a number"))
        else return Number(atom)
    }

    // Returns a boolean from atom or throw an error
    function toLogic(atom) {
        if (typeof atom !== 'string')
            throw new Error(atom + " " + qsTr("is not a logic value"))
        if (atom === 'true') return true
        if (atom === 'false') return false
        throw new Error(atom + " " + qsTr("is not a logic value"))
    }

    // Read color from tree, either as logo color index, svg color name or color RGB
    function* evalColor(tree) {
        if (!tree.length){
            throw new Error(qsTr("Missing color parameters"))
        }
        var atom = yield* evaluate(tree)
        var returnValue =""
        if (Array.isArray(atom)) {
            returnValue = yield* reduce(atom)
        } else {
            if (atom.match(regexNumberLiteral)) {                   // if atom is a number : color indexed
                return turtle.getIndexedColor(atom)
            } else if (atom.match(/^\w+$/)) {                       // if atom is a string : a named color is expected
                return turtle.getNamedColor(atom)
            } else {
                throw new Error(atom + " " + qsTr("is not a color parameter"))
            }
        }
        return returnValue
    }

    // Returns a block filled with evaluated words
    function* reduce(tree) {
        if (!Array.isArray(tree)) return tree
        var returnValue = []
        while (tree.length) {
            returnValue.push(yield* evaluate(tree))
        }
        return returnValue
    }

    // Returns variable's value. Search from maximum varStack depth down to 0
    function* getVar(atom) {
        atom = atom.toUpperCase()
        for (var depth = varStack.length - 1; depth >= 0; depth--) {
            if (Object.keys(varStack[depth]).indexOf(atom) !== -1) {
                if (Array.isArray(varStack[depth][atom]))
                    return copyBlock(varStack[depth][atom])    // Clone variable
                else
                    return varStack[depth][atom]
            }
        }
        throw new Error(qsTr("Unknow variable:") + " " + atom)
    }

    // Returns word or procedure value
    function* getWord(atom, tree) {
        atom = atom.toUpperCase()
        var returnValue = ""
        if (Object.keys(language).indexOf(atom) !== -1) {               // Check for logo keywords
            returnValue = yield* language[atom](tree)                   // Execute logo keyword
        } else if (Object.keys(procedures).indexOf(atom) !== -1) {      // Check for procedures
            var proc = copyBlock(procedures[atom])                      // Clone procedure
            returnValue = yield* runProcedure(tree, proc)               //   and run
        } else {
            throw new Error(qsTr("Unknown word:") + " " + atom)
        }
        return returnValue
    }

    // This is the main function called for each word
    function* evaluate(tree) {
        if (!tree.length) return ""
        var atom = tree.shift()
        var returnValue = ""
//        console.warn(atom/*, tree*/)
        if (Array.isArray(atom)) {                                  // if atom is an array
            returnValue = yield* reduce(atom)
        } else if (atom.match(regexIdentifier)) {                   // if atom is an identifier (logo keyword or procedure name)
            returnValue = yield* getWord(atom, tree)
        } else if (atom.match(regexVariableLiteral)) {              // if atom is a variable (start with ':')
            returnValue = yield* getVar(atom)
        } else if (atom.match(regexStringLiteral)) {                // if atom is a string literal (start with '"')
            returnValue = String(atom).substr(1)
        } else if (atom.match(regexNumberLiteral)) {                // if atom is a number
            returnValue = atom
        } else {
            throw new Error(qsTr("Unknown word:") + " " + atom)
        }
        return returnValue
    }

    function* evalLoop(tree) {
        while (tree.length) {
            if (!sleeper.running) {
                var returnedValue = ""
                returnedValue = yield* evaluate(tree)
            } else {
                yield
            }
        }
        return returnedValue
    }

    function* mainLoop(tree) {
        try {
            yield* evalLoop(tree)
        } catch(err) {
            printError(err)
        }
    }

    function startCode(code) {
        varStack = [{}]
        procedures = {}
        gc()                                        // Garbage collector
        code = code.replace(/;.*\n/g, ' ')          // Filter comments
        code = code.replace(/(\[|\])/g," $1 ")      // Put spaces around "[" and "]" before splitting
        var words = code.trim().split(/\s+/)
        var tree = buildTree(words)
        paused = false
        codeIterator = mainLoop(tree)
        running = true
        scheduler.start()
    }

    // Called by scheduler or step button. Iterate next function on top of iterators stack.
    // Call next() on top of stack and pop when done.
    function schedule(n = 1000) {
        for (var i=0; i < n; i++) {                 // Repeat 1000 times until an animation start (speed up drawing when no animation)
            if (sleeper.running) return
            if (turtle.running) return              // Wait for turtle animation end
            if (codeIterator.next().done) {         // Execute next iteration and returns true when done
                endCode()
                return
            }
        }
    }

    // Called when program terminates, on error or on abort
    function endCode() {
        running = false
        paused = false
        sleeper.stop()
        scheduler.stop()
    }

    function pause() {
        paused = !paused
        if (paused)
            scheduler.stop()
        else
            scheduler.start()
    }

    function stop() {
        printError(qsTr("Aborted"))
    }

    //----------------------------------------------------------------------
    // getEvaluatedType works on an evaluated value. Only string, number as a string or array are identified
    function getEvaluatedType(atom) {
        if (Array.isArray(atom)) {                      // if atom is an array
            return "block"
        } else if (atom.match(regexNumberLiteral)) {    // if atom is a number
            return "number"
        } else {
            return "string"
        }
    }

    // Comparison operator. Equality for numbers, strings and blocks.
    function equal(a, b) {
        if (getEvaluatedType(a) !== getEvaluatedType(b)) return false;
        switch (getEvaluatedType(a)) {
        case 'number':
            return Number(a) === Number(b)
        case 'string':
            return a === b
        case 'block':
            if (a.length !== b.length)
                return false
            for (var i = 0; i < a.length; ++i) {
                if (!equal(a[i], b[i]))
                    return false
            }
            return true
        }
        return false
    }

    // Variables creation. Add to varStack. Called by make_ and localmake_
    function* makeVar(tree, depth) {                  // Create or update a variable
        var name = yield* evaluate(tree)
        var returnedValue = ""
        if (typeof name === 'string') {
            if (Array.isArray(tree[0])) {
                returnedValue = copyBlock(tree.shift())
            } else {
                returnedValue = yield* evaluate(tree)
            }
            varStack[depth][":" + name.toUpperCase()] = returnedValue
        } else {
            throw new Error(name + " " + qsTr("is not a string"))
        }
        return returnedValue
    }

    // Load and run a procedure
    function* runProcedure(tree, proc) {
        varStack.push({})               // New object containing variables for this procedure depth
        while (proc.length) {
            var atom = proc.shift()
            if (atom.match(regexVariableLiteral)) { // Check for procedure's parameters
                if (!tree.length)
                    break
                tree.unshift(atom.replace(/^:/,'"'))
                yield* localmake_(tree) // Build a local variable for parameter
            } else {                    // Variables finished, procedure's body follows
                proc.unshift(atom)      // Restores atom at the beggining of the tree. It's the first word of proc
                break
            }
        }
//        console.warn(JSON.stringify(varStack))
        var returnValue = yield* evalLoop(proc) // Evaluate procedure
        return returnValue                      // Yielded value can't be returned directly. Store it and return after yield terminates
    }

    //--------------------------------------------------------
    // Logo words start here
    //--------------------------------------------------------
    // Create or update a procedure
    function* to_(tree) {
        if (!tree.length) throw new Error(qsTr("Missing procedure name"))
        var name = tree.shift()
        var proc = []
        while (tree.length) {               // Copy procedure's code to END word
            var atom = tree.shift()
            proc.push(atom)
            if ((typeof atom === 'string') && (atom.toUpperCase() === 'END')) {
                break
            }
        }
        procedures[name.toUpperCase()] = proc
    }

    // Stops procedure and returns a value
    function* output_(tree) {
        var returnValue = yield* evaluate(tree)
        while (tree.length) tree.shift()    // Empty the tree before exit
        varStack.pop()
        return returnValue
    }

    // End of procedure
    function* end_(tree) {
        varStack.pop()
        return ""
    }

    function* wait_(tree) {                 // Stop scheduler and start sleeper timer
        scheduler.stop()
        sleeper.interval = toNumber(yield* evaluate(tree)) * 16
        sleeper.start()
        return ""
    }

    //--------------------------------------------------------
    // Variables creation
    function* make_(tree) {                 // Create or update a variable
        var returnedValue = yield *makeVar(tree, 0)
        return returnedValue
    }

    //--------------------------------------------------------
    // Local variables creation
    function* localmake_(tree) {            // Create or update a local variable
        var returnedValue = yield *makeVar(tree, varStack.length - 1)
        return returnedValue
    }

    //--------------------------------------------------------
    // Logo control structure
    function* repeat_(tree) {
        try {
            var count = yield* evaluate(tree)
            if (Number.isNaN(count))
                throw new Error(count + " " + qsTr("is not a number"))
            if (count < 0)
                throw new Error(qsTr(count + " repeat count is negative"))
            if (Array.isArray(tree[0])) {                           // if atom is an array
                var instructions = tree.shift()
                for (var i = 0; i < count; i++) {
                    yield* evalLoop(copyBlock(instructions))
                }
            } else {
                throw new Error(qsTr("REPEAT block expected"))
            }
        } catch(err) {
            printError(err)
        }
        return ""
    }

    function* while_(tree) {
        var condition, instructions
        if (Array.isArray(tree[0])) {
            condition = tree.shift()
        } else {
            throw new Error(qsTr("WHILE condition should be a block"))
        }
        if (Array.isArray(tree[0])) {
            instructions = tree.shift()
        } else {
            throw new Error(qsTr("WHILE instructions should be a block"))
        }
        var returnedValue = ""
        while ((returnedValue = yield* evaluate(copyBlock(condition))) === 'true') {
            yield* evalLoop(copyBlock(instructions))
        }
        return ""
    }

    function* until_(tree) {
        var condition, instructions
        if (Array.isArray(tree[0])) {
            condition = tree.shift()
        } else {
            throw new Error(qsTr("UNTIL condition should be a block"))
        }
        if (Array.isArray(tree[0])) {
            instructions = tree.shift()
        } else {
            throw new Error(qsTr("UNTIL instructions should be a block"))
        }
        var returnedValue = ""
        while ((returnedValue = yield* evaluate(copyBlock(condition))) !== 'true') {
            yield* evalLoop(copyBlock(instructions))
        }
        return ""
    }

    function* if_(tree) {
        if (!tree.length) throw new Error(qsTr("IF condition missing"))
        var condition
        if (Array.isArray(tree[0])) {
            condition = yield* evaluate(tree.shift())
        } else {
            condition = yield* evaluate(tree)
        }
        if (Array.isArray(tree[0])) {
            if (condition === 'true') {     // Everything is a string, even booleans
                yield* evalLoop(tree.shift())
            } else {
                tree.shift()                // skip bloc, condition is not met
            }
        } else {
            throw new Error(qsTr("IF block is missing"))
        }
        return ""
    }

    function* ifelse_(tree) {
        if (!tree.length) throw new Error(qsTr("IFELSE condition missing"))
        var condition
        if (Array.isArray(tree[0])) {
            condition = yield* evaluate(tree.shift())
        } else {
            condition = yield* evaluate(tree)
        }
        if (Array.isArray(tree[0])) {
            if (condition === "true") {     // Everything is text, even booleans
                if (Array.isArray(tree[1])) {
                    tree.splice(1, 1)       // skip else bloc
                } else {
                    throw new Error(qsTr("IFELSE first block missing"))
                }
                yield* evalLoop(tree.shift())
            } else {
                tree.shift()                // skip then bloc
                if (Array.isArray(tree[0])) {
                    yield* evalLoop(tree.shift())
                } else {
                    throw new Error(qsTr("IFELSE second block missing"))
                }
            }
        } else {
            throw new Error(qsTr("IFELSE first block missing"))
        }
        return ""
    }

    //--------------------------------------------------------
    // Output functions
    function printError(aString) {
        output.append("<font color='red'>" + String(aString).replace("Error:", "") + "</font>")
        endCode()
        return ""
    }
    function* clearText_(tree) {
        output.clear()
        output.cursorPosition = 0
        return ""
    }
    function print__(aString)           { output.append(aString) }
    function* print_(tree) {
        var returnValue
        if (Array.isArray(tree[0])) {
            returnValue = yield* reduce(tree.shift())
            output.append(returnValue.join(' '))
        } else {
            returnValue = yield* evaluate(tree)
            if (Array.isArray(returnValue)) {
                returnValue = yield* reduce(returnValue)
                output.append(returnValue.join(' '))
            } else {
                output.append(returnValue)
            }
        }
        return ""
    }

    //--------------------------------------------------------
    // Arithmetic functions
    function* sum_(tree)                 { return String(toNumber(yield* evaluate(tree)) + toNumber(yield* evaluate(tree))) }
    function* difference_(tree)          { return String(toNumber(yield* evaluate(tree)) - toNumber(yield* evaluate(tree))) }
    function* product_(tree)             { return String(toNumber(yield* evaluate(tree)) * toNumber(yield* evaluate(tree))) }
    function* quotient_(tree)            { return String(toNumber(yield* evaluate(tree)) / toNumber(yield* evaluate(tree))) }
    function* remainder_(tree)           { return String(toNumber(yield* evaluate(tree)) % toNumber(yield* evaluate(tree))) }
    function* round_(tree)               { return String(Math.round(toNumber(yield* evaluate(tree)))) }
    function* abs_(tree)                 { return String(Math.abs(toNumber(yield* evaluate(tree)))) }
    function* random_(tree)              { return String(Math.round(Math.random() * toNumber(yield* evaluate(tree)))) }
    function* sqrt_(tree)                { return String(Math.sqrt(toNumber(yield* evaluate(tree)))) }
    function* sin_(tree)                 { return String(Math.sin(toNumber(yield* evaluate(tree)) * Math.PI / 180.0)) }
    function* cos_(tree)                 { return String(Math.cos(toNumber(yield* evaluate(tree)) * Math.PI / 180.0)) }
    function* power_(tree)               { return String(Math.pow(toNumber(yield* evaluate(tree)), toNumber(yield* evaluate(tree)))) }
    function* log10_(tree)               { return String(Math.log10(toNumber(yield* evaluate(tree)))) }
    function* minus_(tree)               { return String(-toNumber(yield* evaluate(tree))) }

    //--------------------------------------------------------
    // Comparison functions
    function* equal_(tree)               { return String(equal(yield* evaluate(tree), yield* evaluate(tree))) }
    function* greater_(tree)             { return String(toNumber(yield* evaluate(tree)) > toNumber(yield* evaluate(tree))) }
    function* greaterEqual_(tree)        { return String(toNumber(yield* evaluate(tree)) >= toNumber(yield* evaluate(tree))) }
    function* less_(tree)                { return String(toNumber(yield* evaluate(tree)) < toNumber(yield* evaluate(tree))) }
    function* lessEqual_(tree)           { return String(toNumber(yield* evaluate(tree)) <= toNumber(yield* evaluate(tree))) }

    //--------------------------------------------------------
    // Logic operators
    function* and_(tree)                 { return String(toLogic(yield* evaluate(tree)) && toLogic(yield* evaluate(tree))) }
    function* or_(tree)                  { return String(toLogic(yield* evaluate(tree)) || toLogic(yield* evaluate(tree))) }
    function* not_(tree)                 { return String(!toLogic(yield* evaluate(tree))) }

    //--------------------------------------------------------
    // Turtle functions
    function* forward_(tree)             { yield turtle.forward(yield* evaluate(tree)) }
    function* back_(tree)                { yield turtle.back(yield* evaluate(tree)) }
    function* right_(tree)               { yield turtle.turnRight(yield* evaluate(tree)) }
    function* left_(tree)                { yield turtle.turnLeft(yield* evaluate(tree)) }
    function* clearScreen_(tree)         { yield turtle.clearScreen() }
    function* home_(tree)                { return turtle.home() }
    function* clean_(tree)               { return turtle.clean() }
    function* heading_(tree)             { return turtle.getHeading() }
    function* setHeading_(tree)          { yield turtle.setHeading(yield* evaluate(tree)) }
    function* setPenWidth_(tree)         { yield turtle.setPenWidth(yield* evaluate(tree)) }
    function* penUp_(tree)               { return turtle.penUp() }
    function* penDown_(tree)             { return turtle.penDown() }
    function* setXY_(tree)               { yield turtle.setXY(yield* evaluate(tree), yield* evaluate(tree)) }
    function* circle_(tree)              { yield turtle.circle(yield* evaluate(tree)) }
    function* xCor_(tree)                { return turtle.xCor() }
    function* yCor_(tree)                { return turtle.yCor() }
    function* showTurtle_(tree)          { return turtle.showTurtle() }
    function* hideTurtle_(tree)          { return turtle.hideTurtle() }
    function* getBackgroundColor_(tree)  { yield* reduce(turtle.getBackgroundColor()) }
    function* setPenColor_(tree) {
        if (!tree.length) throw new Error(qsTr("Missing pen color parameter"))
        var cols = yield* evalColor(tree)
        return turtle.setPenColor(toNumber(cols[0]), toNumber(cols[1]), toNumber(cols[2]))
    }
    function* setBackgroundColor_(tree) {
        if (!tree.length) throw new Error(qsTr("Missing background color parameter"))
        var cols = yield* evalColor(tree)
        return turtle.setBackgroundColor(toNumber(cols[0]), toNumber(cols[1]), toNumber(cols[2]))
    }

    Component.onCompleted: {
        // Text output
        language[qsTr('PRINT')]             = print_
        language[qsTr('PR')]                = print_
        language[qsTr('CLEARTEXT')]         = clearText_
        language[qsTr('CT')]                = clearText_

        // Control structures
        language[qsTr('REPEAT')]            = repeat_
        language[qsTr('IF')]                = if_
        language[qsTr('IFELSE')]            = ifelse_
        language[qsTr('WHILE')]             = while_
        language[qsTr('UNTIL')]             = until_

        // Variables and procedures definitions
        language[qsTr('MAKE')]              = make_
        language[qsTr('LOCALMAKE')]         = localmake_
        language[qsTr('TO')]                = to_
        language[qsTr('OUTPUT')]            = output_
        language[qsTr('END')]               = end_
        language[qsTr('WAIT')]              = wait_

        // Arithmetic operators
        language[qsTr('SUM')]               = sum_
        language[qsTr('DIFFERENCE')]        = difference_
        language[qsTr('PRODUCT')]           = product_
        language[qsTr('QUOTIENT')]          = quotient_
        language[qsTr('REMAINDER')]         = remainder_
        language[qsTr('MODULO')]            = remainder_
        language[qsTr('ROUND')]             = round_
        language[qsTr('ABS')]               = abs_
        language[qsTr('RANDOM')]            = random_
        language[qsTr('SQRT')]              = sqrt_
        language[qsTr('SIN')]               = sin_
        language[qsTr('COS')]               = cos_
        language[qsTr('POWER')]             = power_
        language[qsTr('LOG10')]             = log10_
        language[qsTr('MINUS')]             = minus_

        // Comparison operators
        language[qsTr('EQUAL?')]            = equal_
        language[qsTr('EQUALP')]            = equal_
        language[qsTr('GREATER?')]          = greater_
        language[qsTr('GREATERP')]          = greater_
        language[qsTr('GREATEREQUAL?')]     = greaterEqual_
        language[qsTr('GREATEREQUALP')]     = greaterEqual_
        language[qsTr('LESS?')]             = less_
        language[qsTr('LESSP')]             = less_
        language[qsTr('LESSEQUAL?')]        = lessEqual_
        language[qsTr('LESSEQUALP')]        = lessEqual_

        // Logic operators
        language[qsTr('AND')]               = and_
        language[qsTr('OR')]                = or_
        language[qsTr('NOT')]               = not_

        //--------------------------------------------------------
        // Turtle commands
        language[qsTr('FORWARD')]           = forward_
        language[qsTr('FD')]                = forward_
        language[qsTr('BACK')]              = back_
        language[qsTr('BK')]                = back_
        language[qsTr('RIGHT')]             = right_
        language[qsTr('RT')]                = right_
        language[qsTr('LEFT')]              = left_
        language[qsTr('LT')]                = left_
        language[qsTr('HOME')]              = home_
        language[qsTr('CLEAN')]             = clean_
        language[qsTr('CLEARSCREEN')]       = clearScreen_
        language[qsTr('CS')]                = clearScreen_
        language[qsTr('HEADING')]           = heading_
        language[qsTr('SETHEADING')]        = setHeading_
        language[qsTr('SETH')]              = setHeading_
        language[qsTr('SETPENSIZE')]        = setPenWidth_
        language[qsTr('SETPENWIDTH')]       = setPenWidth_
        language[qsTr('PENUP')]             = penUp_
        language[qsTr('PU')]                = penUp_
        language[qsTr('PENDOWN')]           = penDown_
        language[qsTr('PD')]                = penDown_
        language[qsTr('SETXY')]             = setXY_
        language[qsTr('CIRCLE')]            = circle_
        language[qsTr('SETPENCOLOR')]       = setPenColor_
        language[qsTr('PC')]                = setPenColor_
        language[qsTr('XCOR')]              = xCor_
        language[qsTr('YCOR')]              = yCor_
        language[qsTr('SHOWTURTLE')]        = showTurtle_
        language[qsTr('ST')]                = showTurtle_
        language[qsTr('HIDETURTLE')]        = hideTurtle_
        language[qsTr('HT')]                = hideTurtle_
        language[qsTr('SETBACKGROUND')]     = setBackgroundColor_
        language[qsTr('SETSCREENCOLOR')]    = setBackgroundColor_
        language[qsTr('SETSC')]             = setBackgroundColor_
        language[qsTr('SCREENCOLOR')]       = getBackgroundColor_
    }
}
