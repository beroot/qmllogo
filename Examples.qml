/* LogoDemo - Examples.qml
 *
 * Copyright (C) 2023 Bruno ANSELME <be.root@free.fr>
 *
 * Authors:
 *   Bruno ANSELME <be.root@free.fr> (Qt Quick native)
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
import QtQuick 2.12

ListModel {
    ListElement {
        name: qsTr("Print test")
        code: "CLEARTEXT\nPRINT \"Hello\nPrint [ \"Hello \"world ]\nprint [ 35 \"+ 7 ]\nPRINT SUM 35 7\nPRINT [\n  35 \"+ 7 \":\n  SUM 17 25 \n  \"is \"the \"answer\n]\n"
    }
    ListElement {
        name: qsTr("Arithmetic operators")
        code: "CLEARTEXT\n\nPRINT \"--Integers\nPRINT [\n  SUM 35 7\n  DIFFERENCE 155 113\n  PRODUCT 3 14\n  QUOTIENT 882 21\n  REMAINDER 210 56\n]\nPRINT ABS DIFFERENCE 113 155\nPRINT RANDOM 1000\n\nPRINT \"--Floats\nPRINT SUM 35.12 7.30\nPRINT DIFFERENCE 113.13 70.71\nPRINT PRODUCT 3.2 14.5\nPRINT QUOTIENT 88.5 12.3\nPRINT ROUND QUOTIENT 88.5 12.3\n\nPRINT \"--Other\nPRINT MINUS 5\nPRINT [ \"Square \"root \"2: SQRT 2 ]\nPRINT [ \"Sinus: \n  SIN 0 \"- SIN 30 \"- SIN 90 \n]\nPRINT [ \"Cosinus: \n  COS 0 \"- COS 60 \"- SIN COS 90\n]\nPRINT [ \"Power \"of \"2:\n  POWER 2 0\n  POWER 2 1\n  POWER 2 2\n  POWER 2 3\n  POWER 2 4\n  POWER 2 5\n]\n"
    }
    ListElement {
        name: qsTr("Logic operators")
        code: "CLEARTEXT\nPRINT \"--And\nPRINT AND \"true \"true\nPRINT AND \"true \"false\nPRINT AND \"false \"true\nPRINT AND \"false \"false\n\nPRINT \"--Or\nPRINT OR \"true \"true\nPRINT OR \"true \"false\nPRINT OR \"false \"true\nPRINT OR \"false \"false\n\nPRINT \"--Not\nPRINT NOT \"true\nPRINT NOT \"false\n\nPRINT \"--And\nPRINT AND EQUAL? 1 1 EQUAL? 0 0\nPRINT AND EQUAL? 1 0 EQUAL? 0 0\nPRINT AND EQUAL? 1 1 \"true\nPRINT \"--Not\nPRINT NOT AND EQUAL? 1 0\n\nIFELSE [AND EQUAL? 1 1 EQUAL? 0 0][\n  PRINT [ \"Same ]\n][\n  PRINT [ \"Different ]\n]\nIFELSE [AND EQUAL? 0 1 EQUAL? 0 0][\n  PRINT [ \"Same ]\n][\n  PRINT [ \"Different ]\n]\n"
    }
    ListElement {
        name: qsTr("Variables")
        code: "CT\nMAKE \"NAME \"Myself\nMAKE \"TXT1 [ \"Hello \"crazy \"world ]\nMAKE \"TXT2 [ \"Hello :NAME ]\nMAKE \"VAR1 35\nMAKE \"VAR2 18\nMake \"TOTAL sum :var1 :var2\n\nPRINT \"Hello\nprint :TXT1\nprint :TXT2\nPrint :VAR1\nprint :var2\nprint :TOTAL\nPRINT [ \"Total \": SUM :VAR1 :VAR2 ]\n\n"
    }
    ListElement {
        name: qsTr("Test structures")
        code: "CLEARTEXT\n\nPRINT [ \"-- \"Test \"IF ]\nIF EQUAL? 0 0 [ PRINT \"Equality ]\nIF EQUAL? 0 1 [ PRINT \"Equality ]\nif [ equal? 5 sum 3 2 ] [\n  Print \"Equality\n]\nprint equal? 0 0\nprint equal? 0 1\n\nPRINT [ \"-- \"Test \"IFELSE ]\nIFELSE EQUAL? 1 0 [ \n  PRINT \"Equality\n] [\n  print \"Inequality\n]\nto check :a :b \n  ifelse equal? :a :b [\n    print [ :a \"and :b \"are \"equal]\n  ][\n    print [ :a \"and :b \"are \"different]\n  ]\nend\n\ncheck 0 0\ncheck 10 0\ncheck \"tata \"tata\ncheck \"tata \"titi\n"
    }
    ListElement {
        name: qsTr("Comparison operators")
        code: "CLEARSCREEN CLEARTEXT\nPRINT \"--Equality\nPRINT EQUAL? 0 1\nPRINT EQUAL? 0 0\nPRINT EQUAL? 1 1\nPRINT EQUAL? 1 DIFFERENCE 3 3\nPRINT EQUALP \"Toto \"Tata\nPRINT EQUALP \"Tata \"Tata\nPRINT EQUALP 1 \"Tata\nprint equal? [ 0 1 2 ] [ 0 1 2 ]\nprint equal? [ 0 1 2 ] [ 0 1 ]\nprint [ \"block equal? [ 0 1 2 ] [ 0 1 DIFFERENCE 3 1 ] ]\n\nPRINT \"--Greater\nPRINT GREATER? 0 1\nPRINT GREATER? 1 1\nPRINT GREATER? 2 1\n\nPRINT \"--Less\nPRINT LESS? 0 1\nPRINT LESS? 1 1\nPRINT LESSEQUAL? 1 1\nPRINT LESS? 2 1\n"
    }
    ListElement {
        name: qsTr("Turtle motions")
        code: "CLEARSCREEN CT\nSETPENWIDTH 3\n\n; Letter L\nSETXY -220 0\nFORWARD 100\nBACK 100\nRIGHT 90\nFORWARD 60\nLEFT 90\n\n; Letter O\nSETXY -110 -50\nCIRCLE 50\n\n; Letter G\nSETXY 20 -50\nRIGHT 90\nFD 20\nRIGHT 90\nFD 50 RT 90\nFD 80 RT 90\nFD 100 RT 90\nFD 80\n\n; Letter O\nSETXY 120 -50\nSETHEADING 0\nCIRCLE 50\n"
    }
    ListElement {
        name: qsTr("Repeat loop")
        code: "CLEARSCREEN CLEARTEXT\nREPEAT 3 [\n  FORWARD 120\n  RIGHT 60\n  FORWARD 60\n  RIGHT 60\n  PRINT [ XCOR YCOR HEADING ]\n]\n"
    }
    ListElement {
        name: qsTr("Nested repeat loops")
        code: "CLEARSCREEN CT\nREPEAT 24 [\n  REPEAT 3 [\n    FD 120\n    RIGHT 60\n    FD 60\n    RIGHT 60\n  ]\n  LEFT 15\n]\nPRINT [ XCOR YCOR HEADING ]\n"
    }
    ListElement {
        name: qsTr("Colors format")
        code: "CLEARSCREEN CT\nLEFT 90\nSETPENWIDTH 2\n; indexed color 0..15\nSETPENCOLOR 4\nREPEAT 36 [\n  FD 5 RT 10\n]\n; named svg color\nSETPENCOLOR  \"darkKhaki\nREPEAT 180 [\n  FD 2 RT 2\n]\n; red, green, blue\nSETPENCOLOR [ 255 127 0 ]\nREPEAT 72 [\n  FD 10 RT 5\n]\n; rgb with parameter\nREPEAT 72 [\n  SETPENCOLOR [\n    PRODUCT 5 HEADING 0 255 \n  ]\n  FD 13 RT 5\n]\nPRINT HEADING\n"
    }
    ListElement {
        name: qsTr("Indexed colors")
        code: "CLEARSCREEN CLEARTEXT\nLEFT 90\nSETPENWIDTH 3\nMAKE \"COL 0\nREPEAT 16 [\n  SETPENCOLOR :COL ; indexed color\n  REPEAT 36 [\n    FD SUM :COL 5 RT 10\n  ]\n  MAKE \"COL SUM :COL 1\n]\n"
    }
    ListElement {
        name: qsTr("Loop with variables")
        code: "CLEARSCREEN CT\nSETSCREENCOLOR \"antiquewhite\nmake \"COL 0\nmake \"d 10\nrepeat 54 [\n  setpencolor :col\n  repeat 3 [\n    fd :d\n    right 60\n    fd quotient :d 2\n    right 60\n  ]\n  left 10\n  make \"d sum :d 2\n  make \"col modulo sum :col 1 16\n]\n"
    }
    ListElement {
        name: qsTr("Procedure with no parameter")
        code: "CLEARSCREEN CT\nSETXY -300 0\nTO SQUARE\n  REPEAT 4 [ FD 80 RT 90 ]\nEND\nREPEAT 6 [\n  SQUARE\n  PENUP\n  RIGHT 90\n  FORWARD 90\n  LEFT 90\n  PENDOWN\n]\n"
    }
    ListElement {
        name: qsTr("Procedure one parameter")
        code: "CLEARSCREEN CT\nto star :n\n  repeat 5 [ fd :n rt 144 ]\nend\nsetpenwidth 2\n\nsetpencolor \"burlywood\nsetxy -150 -150\nstar 100\nsetpencolor \"chartreuse\nsetxy -150 150\nstar 200\nsetpencolor \"deepPink\nsetxy 150 150\nstar 150\nsetpencolor \"lightblue\nsetxy 150 -150\nstar 50\n"
    }
    ListElement {
        name: qsTr("Procedure two parameters")
        code: "CLEARSCREEN CLEARTEXT\nto star :dist :color\n  setpencolor :color\n  repeat 5 [\n    fd :dist \n    rt 144 \n  ]\nend\nsetpenwidth 2\nmake \"size 100\nmake \"col 0\nrepeat 16 [\n  star :size :col\n  rt 22\n  make \"size sum :size 10\n  make \"col sum :col 1\n]\n"
    }
    ListElement {
        name: qsTr("Procedure return value")
        code: "CS CT\nto star :n\n  repeat 5 [ fd :n rt 144 ]\n  output :n\nend\nsetpenwidth 2\n\nsetpencolor \"burlywood\nsetxy -150 -150\nprint [ \"Size: star 100 ]\nsetpencolor \"chartreuse\nsetxy -150 150\nprint [ \"Size:  star 200 ]\nsetpencolor \"deepPink\nsetxy 150 150\nprint [ \"Size:  star 150 ]\nsetpencolor \"lightblue\nsetxy 150 -150\nprint [ \"Size:  star 50 ]\n"
    }
    ListElement {
        name: qsTr("Colors check")
        code: "CS CT\nSETXY -300 0\nTO SQUARE\n  REPEAT 4 [ FD 80 RT 90 ]\nEND\n\nTO DEPLACE\n  PENUP \n  RT 90 \n  FD 90 \n  LT 90 \n  PENDOWN\nEND\n\n\nMAKE \"VAL 200\n\nSETPENCOLOR 5\nSQUARE DEPLACE\nSETPENCOLOR \"burlywood ;222,184,135\nSQUARE DEPLACE\nSETPENCOLOR [127 255 127 ]\nSQUARE DEPLACE\nSETPENCOLOR [:VAL 255 127 ]\nSQUARE DEPLACE\nSETPENCOLOR [255 :VAL 127 ]\nSQUARE DEPLACE\nSETPENCOLOR [255 127 :VAL ]\nSQUARE DEPLACE\n"
    }
    ListElement {
        name: qsTr("Draw circles")
        code: "CS CT\nMAKE \"POSITION -250\nSETPENWIDTH 3\nPRINT HEADING\nMAKE \"DIST 20\nREPEAT 50 [\n  SETXY :POSITION 0\n  SETPENCOLOR [ 200 0 QUOTIENT :DIST 3 ]\n  CIRCLE QUOTIENT :DIST 4\n  MAKE \"DIST SUM :DIST 20\n  MAKE \"POSITION SUM :POSITION 5\n]\nPRINT SCREENCOLOR\n"
    }
    ListElement {
        name: qsTr("Other circles")
        code: "CS CT\nSETXY -50 0\nSETPENWIDTH 3\nPRINT HEADING\nMAKE \"DIST 20\nREPEAT 60 [\n  SETPENCOLOR [ 255 HEADING 0 ]\n  FD PRODUCT :DIST 0.75\n  CIRCLE QUOTIENT :DIST 4\n  BK :DIST LT 12\n  MAKE \"DIST SUM :DIST 3\n]\nPRINT SCREENCOLOR\n"
    }
    ListElement {
        name: qsTr("Random stars")
        code: "CS CLEARTEXT\nSETSCREENCOLOR \"black\nTO STAR :N\n  REPEAT 5 [ FD :N RT 144 ]\nEND\n\nSETPENWIDTH 2\nREPEAT 30 [\n  SETPENCOLOR RANDOM 16\n  SETXY \n    DIFFERENCE RANDOM 600 300\n    DIFFERENCE RANDOM 600 300\n  SETHEADING RANDOM 360\n  STAR SUM RANDOM 50 5\n]\nHIDETURTLE\n\n"
    }
    ListElement {
        name: qsTr("While loop")
        code: "CS CT\nSETSCREENCOLOR \"antiquewhite\nmake \"COL 0\nmake \"d 100\nwhile [ less? :d 200 ] [\n  setpencolor :col\n  repeat 3 [\n    fd :d\n    right 60\n    fd quotient :d 2\n    right 60\n  ]\n  left 10\n  make \"d sum :d 2\n  make \"col modulo sum :col 1 16\n]\n"
    }
    ListElement {
        name: qsTr("Until loop")
        code: "CS CT\nSETSCREENCOLOR \"antiquewhite\nmake \"COL 0\nmake \"d 50\nuntil [ greater? :d 200 ] [\n  setpencolor :col\n  repeat 4 [\n    fd :d\n    right 45\n    fd quotient :d 2\n    right 45\n  ]\n  right 10\n  make \"d sum :d 2\n  make \"col modulo sum :col 1 16\n]\n"
    }
    ListElement {
        name: qsTr("Recursivitree")
        code: "CS CT\nSETXY 0 250\n\nSETPENWIDTH 3\nTO TREE :DIST\n  IF GREATER? :DIST 25 [\n    PENDOWN\n    FORWARD :DIST\n    LEFT 30\n    TREE QUOTIENT :DIST 2\n    RIGHT 30\n    TREE QUOTIENT :DIST 1.5\n    RIGHT 30\n    TREE QUOTIENT :DIST 2\n    LEFT 30\n    PENUP\n    BACK :DIST\n  ]\nEND\n\nTREE 200\n"
    }
    ListElement {
        name: qsTr("Color recursivitree")
        code: "CS CT\nSETXY 0 250\n\nTO TREE :DIST\n  IFELSE LESS? :DIST 30 [\n    SETPENCOLOR \"green\n    SETPENWIDTH 2\n  ][\n    SETPENCOLOR \"brown\n    SETPENWIDTH 5\n  ]\n  IF GREATER? :DIST 10 [\n    PENDOWN\n    FORWARD :DIST\n    LEFT 30\n    TREE QUOTIENT :DIST 2\n    RIGHT 25\n    TREE QUOTIENT :DIST 1.5\n    RIGHT 25\n    TREE QUOTIENT :DIST 2\n    LEFT 20\n    PENUP\n    BACK :DIST\n  ]\nEND\n\nTREE 200\n"
    }
    ListElement {
        name: qsTr("Random tree")
        code: "CLEARSCREEN CT\nSETXY 0 350\n\nTO TREE :DIST\n  IFELSE LESS? :DIST 30 [\n    SETPENCOLOR \"green\n    SETPENWIDTH 2\n  ][\n    SETPENCOLOR \"brown\n    SETPENWIDTH 5\n  ]\n  LOCALMAKE \"ANG0 DIFFERENCE RANDOM 40 20\n  LOCALMAKE \"ANG1 SUM RANDOM 20 10\n  LOCALMAKE \"ANG2 SUM RANDOM 20 10\n  IF [ AND GREATER? :DIST 15\n           GREATER? RANDOM :DIST 3 ][\n    FORWARD :DIST\n    LEFT :ANG0\n    LEFT :ANG1\n    TREE PRODUCT :DIST 0.6\n    RIGHT :ANG1\n    TREE PRODUCT :DIST 0.8\n    RIGHT :ANG2\n    TREE PRODUCT :DIST 0.7\n    LEFT :ANG2\n    PENUP\n    RIGHT :ANG0\n    BACK :DIST\n    PENDOWN\n  ]\nEND\n\nTREE 170\n"
    }
    ListElement {
        name: qsTr("Koch curves")
        code: "CS CT\nSETPENWIDTH 3\n\nTO KOCH :DIST :DEPTH\n  LOCALMAKE \"ND DIFFERENCE :DEPTH 1\n  IFELSE LESS? :ND 0 [\n    FORWARD :DIST\n  ][\n    KOCH QUOTIENT :DIST 3 :ND\n    LEFT 60\n    KOCH QUOTIENT :DIST 3 :ND\n    RIGHT 120\n    KOCH QUOTIENT :DIST 3 :ND\n    LEFT 60\n    KOCH QUOTIENT :DIST 3 :ND\n  ]\nEND\n\nSETXY -300 350\nSETHEADING 90\nKOCH 600 0\n\nSETXY -300 300\nSETHEADING 90\nWAIT 60\nKOCH 600 1\n\nSETXY -300 120\nSETHEADING 90\nWAIT 60\nKOCH 600 2\n\nSETXY -300 -50\nSETHEADING 90\nWAIT 60\nKOCH 600 3\n\nSETXY -300 -220\nSETHEADING 90\nWAIT 60\nKOCH 600 4\n"
    }
    ListElement {
        name: qsTr("Koch flake")
        code: "CS CT\n\nTO KOCH :DIST :DEPTH\n  LOCALMAKE \"ND DIFFERENCE :DEPTH 1\n  IFELSE LESS? :ND 0 [\n    FORWARD :DIST\n  ][\n    KOCH QUOTIENT :DIST 3 :ND\n    LEFT 60\n    KOCH QUOTIENT :DIST 3 :ND\n    RIGHT 120\n    KOCH QUOTIENT :DIST 3 :ND\n    LEFT 60\n    KOCH QUOTIENT :DIST 3 :ND\n  ]\nEND\n\nSETXY -300 -150\nSETHEADING 90\nSETPENWIDTH 3\nHIDETURTLE\nREPEAT 3 [\n  KOCH 600 5\n  RIGHT 120\n]\n"
    }
    ListElement {
        name: qsTr("Curly")
        code: "cs\npenup\nforward 150\npendown\nsetpenwidth 3\nsetpencolor \"turquoise\n\nrepeat 4 [\n  make \"x 1\n  while [ less? :x 100 ][\n    forward 30\n    right difference 100 :x\n    make \"x sum :x 1\n  ]\n]\n"
    }
}
