/* DemoLogo.qml
 *
 * Copyright (C) 2023 Bruno ANSELME <be.root@free.fr>
 *
 * Authors:
 *   Bruno ANSELME <be.root@free.fr> (Qt Quick native)
 *
 *   SPDX-License-Identifier: GPL-3.0-or-later
 */
import QtQuick 2.12
import QtQuick.Shapes 1.15
import QtQuick.Controls 2.12

Rectangle {
    id: page
    width: 1200
    height: 900
    focus: true

    Rectangle {
        id: buttons
        width: exosList.width + scrollCode.width
        height: 37
        anchors.top: parent.top
        Row {
            spacing: 4
            Button {
                width: 40
                icon.source: "stop.svg"
                ToolTip.text: qsTr("Abort")
                ToolTip.visible: hovered
                enabled: logo.running
                onClicked: logo.stop()
            }
            Button {
                id: startButton
                width: 40
                icon.source: logo.running ? "pause.svg" : "play.svg"
                ToolTip.text: !logo.running ? qsTr("Run") : logo.paused ? qsTr("Continue") : qsTr("Pause")
                ToolTip.visible: hovered
                onClicked: {
                    if (logo.running) {
                        logo.pause()
                    } else {
                        logo.startCode(sourceCode.text)
                    }
                }
            }
            Button {
                id: stepButton
                width: 40
                icon.source: "next.svg"
                ToolTip.text: qsTr("Step by step")
                ToolTip.visible: hovered
                enabled: !logo.running || logo.paused
                onClicked: {
                    if (!logo.running) {
                        logo.startCode(sourceCode.text)
                        logo.pause()
                    } else {
                        logo.schedule(1)
                    }
                }
            }
            Column {
                Text {
                    text: qsTr("Animation")
                    font.pixelSize: 11
                }
                CheckBox {
                    id: animate
                    anchors.horizontalCenter: parent.horizontalCenter
                    checked: turtle.animate
                    onCheckedChanged: turtle.animate = checked
                }
            }
            Column {
                Text {
                    text: qsTr("Animation speed")
                    font.pixelSize: 11
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                }
                Slider {
                    id: speedSlider
                    from: 1.8
                    to: 4
                    value: turtle.speed
                    snapMode: Slider.SnapAlways
                    width: 120
                    enabled: animate.checked
                    ToolTip.visible: pressed
                    ToolTip.text: qsTr("Turtle speed")
                    onValueChanged: turtle.speed = value
                }
            }
            Button {
                text: qsTr("Quit")
                onClicked: Qt.quit()
            }
        }
    }
    Rectangle {
        id: exosList
        width: 180
        height: 600
        anchors.top: buttons.bottom
        anchors.bottom: scrollOutput.top
        border.width: 1
        border.color: "gray"
        color: "beige"
        Text {
            id: title
            width: parent.width
            height: 20
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            text: qsTr("Logo examples")
        }
        // Examples list
        ListView {
            id: exos
            anchors.top: title.bottom
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 5
            model: Examples {}
            focus: true
            delegate: Text {
                text: name
                MouseArea{
                    anchors.fill: parent
                    onClicked: exos.currentIndex = index
                }
            }
            highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
            highlightFollowsCurrentItem: true
            onCurrentIndexChanged: {
                sourceCode.text = model.get(currentIndex).code
            }
            Component.onCompleted: {
//                currentIndex = 7
            }
        }
    }

    // Code viewer and editor
    ScrollView {
        id: scrollCode
        width: 300
        height: exosList.height
        anchors.top: buttons.bottom
        anchors.bottom: scrollOutput.top
        anchors.left: exosList.right
        TextArea {
            id: sourceCode
            font.family: "fixed"
            font.pixelSize: 12
            focus: true
            text: ""
            wrapMode: TextEdit.WordWrap
        }
    }

    // Turtle area
    Turtle {
        id: turtle
        anchors.top: parent.top
        anchors.left: scrollCode.right
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }
    // Text output
    ScrollView {
        id: scrollOutput
        width: exosList.width + scrollCode.width
        height: 300
        anchors.bottom: parent.bottom
        TextArea {
            id: output
            textFormat: TextEdit.RichText
            text: ""
        }
    }

    Logo {
        id: logo
        turtle: turtle
        output: output
    }

    Keys.onPressed: {
        if (event.modifiers & Qt.ControlModifier) {
            switch (event.key) {
            case Qt.Key_Q :
                Qt.exit(0)
                break
            case Qt.Key_Down :
                startButton.clicked()
                break
            }
        } else {
            switch (event.key) {
            case Qt.Key_Down :
                stepButton.clicked()
                break
            case Qt.Key_Left :
                speedSlider.decrease()
                break
            case Qt.Key_Right :
                speedSlider.increase()
                break
            }

        }
    }
}

